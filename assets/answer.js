

var GameData = require('GameData');
var Global = require('Global');
cc.Class({
    extends: cc.Component,

    properties: {
        index: 0,
    },



    start() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.selectAnswer, this);
    },

    init: function (data) {
        this.tempIndex = "A" + (this.index + 1);
        this.node.getComponent(cc.Label).string = data[this.tempIndex];
    },

    selectAnswer: function () {
        if (GameData.curLevel < GameData.maxLevel) {
            GameData.selectAnswer[GameData.curLevel] = this.tempIndex;
            //GameData.curLevel++;
            GameData.curQs++;
            Global.GameUI.setCurLevel();
        }


    }

});
