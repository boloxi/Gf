
var GameData = require('GameData');
var Global = require('Global');
cc.Class({
    extends: cc.Component,

    properties: {
        QsLabel: cc.Label,
        answerNode: [
            cc.Node
        ],
        resultLabel: cc.Label,
        resultNode: cc.Node,
        timeProgressBar: cc.ProgressBar,
    },

    onLoad: function () {
        var self = this;
        this._update = true;
        Global.GameUI = this;
        cc.loader.loadRes('data/question', function (err, res) {
            var obj = res.json;
            self.QsConfig = obj;
            GameData.maxLevel = obj.length;
            cc.loader.loadRes('data/level', function (err, res) {
                var obj = res.json;
                self.levelConfig = obj;
                console.log(self.levelConfig);
                //self.freshUI();
                self.setCurLevel();
            })

        })
    },

    freshProgress: function () {
        this.timeProgressBar.progress = this.precent / 10;
    },

    setCurLevel: function () {
        console.log("setCurLevel");
        var QsArr = [];
        if (JSON.stringify(GameData.selectAnswer) == "{}") {
            GameData.curLevel = Math.floor(Math.random() * GameData.maxLevel);
        }
        else {

            var randIndex = Math.random() * 100;
            if (randIndex > this.levelConfig[GameData.curQs]) {
                console.log("用新的");
                for (let i = 0; i < GameData.maxLevel; i++) {
                    QsArr.push(i);
                }
                for (let k in GameData.selectAnswer) {
                    let index = QsArr.indexOf(parseInt(k));
                    if (index > -1) {
                        QsArr.splice(index, 1);
                    }
                }

            }
            else {
                console.log("用旧的");
                for (let l in GameData.selectAnswer) {
                    QsArr.push(parseInt(l));
                }
            }
            var curIndex = Math.floor(Math.random() * QsArr.length);
            GameData.curLevel = QsArr[curIndex];
        }
        
        console.log(GameData.curLevel);
        this.freshUI();
    },

    freshUI: function () {
        this.precent = 10;
        var isFind = false;
        for (var i = 0; i < this.QsConfig.length; i++) {
            if (this.QsConfig[i].level == GameData.curLevel) {
                this.freshQs(this.QsConfig[i]);
                this.freshAnswer(this.QsConfig[i]);
                isFind = true;
            }
        }
        if (!isFind) {
            this.showResult();
        }
    },

    freshQs: function (data) {
        this.QsLabel.string = data.Q;
    },

    freshAnswer: function (data) {
        for (var i = 0; i < this.answerNode.length; i++) {
            this.answerNode[i].getComponent("answer").init(data);
        }
    },

    restartGame: function () {

    },

    showResult: function () {
        var tempString = "";
        this.resultNode.active = true;
        for (var i in GameData.selectAnswer) {
            for (var j = 0; j < this.QsConfig.length; j++) {
                if (i == this.QsConfig[j].level) {
                    tempString += this.QsConfig[j].result + this.QsConfig[j][GameData.selectAnswer[i]] + "\n";
                }
            }
        }
        this.resultLabel.string = tempString
    },

    update: function (dt) {
        if (!this._update) {
            return;
        }
        this.precent -= dt;
        if (this.precent <= 0) {
            this.showResult();
        }
        else {
            this.freshProgress();
        }

    }
});
